VERSION = 0.9.9
NAME = mdv-rpm-summary

DIRS = rpm-summary-contrib rpm-summary-main rpm-summary-non-free rpm-summary-restricted

all: dirs

dirs:
	@for n in . $(DIRS); do \
		[ "$$n" = "." ] || make -C $$n || exit 1 ;\
	done

install: $(ALL)
	@for n in $(DIRS); do \
		(cd $$n; $(MAKE) install) \
	done

update: pot_files po_files

# build new pot files
pot_files:
	./rpm2pot
	touch pot_files
	@rm -f */placeholder.h

po_files: pot_files
	for i in main contrib non-free restricted; \
	do \
	  cd rpm-summary-$$i ; \
	  for f in *.po ; \
	  do \
	    cp $$f $$f.old ; \
	    msgmerge $$f.old rpm-summary-$$i.pot > $$f && \
	    rm -f $$f.old || mv $$f.old $$f ; \
	  done ; \
	  cd .. ; \
	done
	touch po_files

clean:
	rm -rf pot_files po_files

dis: clean
	rm -rf $(NAME)-$(VERSION) ../$(NAME)-$(VERSION).tar*
	svn export -q -rBASE . $(NAME)-$(VERSION)
	find $(NAME)-$(VERSION) -name .cvsignore |xargs rm -rf
	tar cf ../$(NAME)-$(VERSION).tar $(NAME)-$(VERSION)
	bzip2 -9f ../$(NAME)-$(VERSION).tar
	rm -rf $(NAME)-$(VERSION)

.PHONY: ChangeLog

log: ChangeLog

changelog: ChangeLog

ChangeLog:
	svn2cl --accum --authors ../common/username.xml
